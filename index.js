// output 1
db.users.find(
    {
        $or:
        [
            {
                firstName: 
                {
                    $regex: 'S'
                }
            },
            {
                lastName:
                {
                    $regex: "D"
                }
            }
        ]
    },
    {
        firstName: 1,
        lastName: 1,
        _id: 0
    }
)

// output 2
db.users.find(
    {
        $and:
        [
            {department: "HR"},
            {age: {$gte: 70}}
        ]
    }
)


// output 3
db.users.find(
    {
        $and:
        [
            {
                firstName: 
                {
                    $regex: 'E',
                    $options: '$i'
                }
            },
            {
                age: {$lte: 30}
            }
        ]
    }
)
